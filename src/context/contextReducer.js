//Reducer => a function that takes in the old state, and an action => new state

const contextReducer = (oldstate, action) => {
	let transactions;

	switch(action.type) {

		case 'DELETE_TRANSACTION':
		   transactions = oldstate.filter((t) => t.id !== action.payload)

		   localStorage.setItem('transactions', JSON.stringify(transactions));
		   return transactions;

		case 'ADD_TRANSACTION':
		     transactions = [action.payload, ...oldstate];

		     localStorage.setItem('transactions', JSON.stringify(transactions));
		return transactions;

		default:
		return oldstate;
	}

} 

export default contextReducer;